const request = require('request');

const extIDs = require('./extIDs.json');
const baseURL = 'https://chrome.google.com/webstore/detail/';
const titleDescriptionRegex = /<meta property="og:title" content="(.*?)">.*<meta property="og:description" content="(.*?)">/;

for (let id of extIDs) {
    request(baseURL + id, (err, res, body) => {
        if (err) {
            console.error(`Something went wrong while trying to fetch extension page with ID ${id}`);
            return;
        }

        if (res.statusCode == 404) {
            console.error(`Invalid ID: ${id} (Page 404ing)`);
            return;
        }

        const matches = body.match(titleDescriptionRegex);

        if (matches == null) {
            console.error(`Something went wrong while matching the title of ID ${id} (this should never happen)`);
            return;
        }

        console.log(`${matches[1]} - ${matches[2]}`);
    });
}
